<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'as' => 'home', 'uses' => 'PagesController@homepage'
]);

Route::get('/contact', [
    'as' => 'contact', 'uses' => 'PagesController@contact'
]);

Route::get('/match', [
    'as' => 'match', 'uses' => 'PagesController@match'
]);

Route::get('/mission', [
    'as' => 'mission', 'uses' => 'PagesController@mission'
]);

Route::get('/pricing', [
    'as' => 'pricing', 'uses' => 'PagesController@pricing'
]);

Route::get('/profile', [
    'as' => 'profile', 'uses' => 'PagesController@profile'
]);

Route::get('/signup', [
    'as' => 'signup', 'uses' => 'PagesController@signup'
]);

Route::post('/signuppost', [
    'as' => 'signuppost', 'uses' => 'PagesController@signuppost'
]);

Route::get('/solution', [
    'as' => 'solution', 'uses' => 'PagesController@solution'
]);

Route::get('/detail', [
    'as' => 'detail', 'uses' => 'PagesController@detail'
]);
