<!DOCTYPE html>
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <title>Cl1que - connecting Business, Investors, and Experts</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/main-color.css" id="colors">

</head>

<body class="">

<!-- Wrapper -->
<div id="wrapper">

    <!-- Header Container
    ================================================== -->
    @include('layout.header')
    <div class="clearfix"></div>
    <!-- Header Container / End -->
    <!-- Banner
    ================================================== -->
    @yield('banner')
    <!-- Content
    ================================================== -->
    <!-- Container -->
    @yield('content')
    <!-- Container / End -->

    <!-- Info Section -->
    <!-- Info Section / End -->

    <!-- Flip banner -->
    @yield('flipBanner')
    <!-- Flip banner / End -->
    <!-- Footer
    ================================================== -->
    @include('layout.footer')
    <!-- Footer / End -->
    <!-- Back To Top Button -->
    <div id="backtotop"><a href="#"></a></div>
</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->
<script type="text/javascript" src="scripts/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="scripts/jquery-migrate-3.1.0.min.js"></script>
<script type="text/javascript" src="scripts/mmenu.min.js"></script>
<script type="text/javascript" src="scripts/chosen.min.js"></script>
<script type="text/javascript" src="scripts/slick.min.js"></script>
<script type="text/javascript" src="scripts/rangeslider.min.js"></script>
<script type="text/javascript" src="scripts/magnific-popup.min.js"></script>
<script type="text/javascript" src="scripts/waypoints.min.js"></script>
<script type="text/javascript" src="scripts/counterup.min.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/tooltips.min.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>


<!-- Google Autocomplete -->
<script>
	function initAutocomplete() {
		var input = document.getElementById('autocomplete-input');
		var autocomplete = new google.maps.places.Autocomplete(input);

		autocomplete.addListener('place_changed', function() {
			var place = autocomplete.getPlace();
			if (!place.geometry) {
				window.alert("No details available for input: '" + place.name + "'");
				return;
			}
		});

		if ($('.main-search-input-item')[0]) {
			setTimeout(function(){
				$(".pac-container").prependTo("#autocomplete-container");
			}, 300);
		}
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&callback=initAutocomplete"></script>


<!-- Typed Script -->
<script type="text/javascript" src="scripts/typed.js"></script>
<script>
	var typed = new Typed('.typed-words', {
		strings: ["Attractions"," Restaurants"," Hotels"],
		typeSpeed: 80,
		backSpeed: 80,
		backDelay: 4000,
		startDelay: 1000,
		loop: true,
		showCursor: true
	});
</script>


<!-- Date Range Picker - docs: http://www.daterangepicker.com/ -->
<script src="scripts/moment.min.js"></script>
<script src="scripts/daterangepicker.js"></script>

<script>
	$(function() {

		var start = moment().subtract(0, 'days');
		var end = moment().add(2, 'days');

		function cb(start, end) {
			$('#booking-date-search').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}
		cb(start, end);
		$('#booking-date-search').daterangepicker({
			"opens": "right",
			"autoUpdateInput": true,
			"alwaysShowCalendars": true,
			startDate: start,
			endDate: end
		}, cb);

		cb(start, end);

	});

	// Calendar animation and visual settings
	$('#booking-date-search').on('show.daterangepicker', function(ev, picker) {
		$('.daterangepicker').addClass('calendar-visible calendar-animated bordered-style');
		$('.daterangepicker').removeClass('calendar-hidden');
	});
	$('#booking-date-search').on('hide.daterangepicker', function(ev, picker) {
		$('.daterangepicker').removeClass('calendar-visible');
		$('.daterangepicker').addClass('calendar-hidden');
	});

	$(window).on('load', function() {
		$('#booking-date-search').val('');
	});
    @yield('script')
</script>



</body>
</html>