<div id="footer" class="sticky-footer">
    <!-- Main -->
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-sm-6">
                <img class="footer-logo" src="images/logo.png" alt="">
                <br><br>
                <p>We're Cl1QUE: the e-Harmony of business - connecting business, investors, and experts. You'll know everything you need to know - <a href="{{route('signup')}}">WITH JUST ONE CLICK</a>.</p>
            </div>
            <div class="col-md-4 col-sm-6 ">
            </div>

            <div class="col-md-3  col-sm-12">
                <h4>Contact Us</h4>
                <div class="text-widget">
                    E-Mail:<span> <a href="#">hello@cl1que.com</a> </span><br>
                </div>

                <ul class="social-icons margin-top-20">
                    <li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
                    <li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
                </ul>

            </div>

        </div>

        <!-- Copyright -->
        <div class="row">
            <div class="col-md-12">
                <div class="copyrights">© 2019 Cl1que. All Rights Reserved.</div>
            </div>
        </div>

    </div>

</div>