@extends('layout.master')
@section('content')
    <!-- Titlebar
    ================================================== -->
    <div id="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>Pricing Tables</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li>Pricing</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>


    <!-- Pricing Tables
    ================================================== -->

    <!-- Container / Start -->
    <div class="container">

        <!-- Row / Start -->
        <div class="row">

            <div class="col-md-12">
                <div class="pricing-container margin-top-30">

                    <!-- Plan #1 -->

                    <div class="plan">

                        <div class="plan-price">
                            <img src="images/micro.png" height="100">
                            <h3>Mirco</h3>
                            <span class="value">Fee Based</span>
                            <span class="period"> <$500,000 </span>
                        </div>

                        <div class="plan-features">
                            <ul>
                                <li>One Listing</li>
                                <li>90 Days Availability</li>
                                <li>Non-Featured</li>
                                <li>Limited Support</li>
                            </ul>
                            <a class="button border" href="#">Get Started</a>
                        </div>

                    </div>

                    <!-- Plan #3 -->
                    <div class="plan featured">

                        <div class="listing-badge">
                            <span class="featured">Featured</span>
                        </div>

                        <div class="plan-price">
                            <img src="images/small.png" height="100">
                            <h3>Small</h3>
                            <span class="value">Fee Based</span>
                            <span class="period"> <$5,000,000</span>
                        </div>
                        <div class="plan-features">
                            <ul>
                                <li>One Time Fee</li>
                                <li>One Listing</li>
                                <li>Lifetime Availability</li>
                                <li>Featured In Search Results</li>
                                <li>24/7 Support</li>
                            </ul>
                            <a class="button" href="#">Get Started</a>
                        </div>
                    </div>

                    <!-- Plan #3 -->
                    <div class="plan">

                        <div class="plan-price">
                            <img src="images/medium.png" height="100">
                            <h3>Medium</h3>
                            <span class="value">Fee Based</span>
                            <span class="period"><$20,000,000</span>
                        </div>

                        <div class="plan-features">
                            <ul>
                                <li>Unlimited Listings</li>
                                <li>Lifetime Availability</li>
                                <li>Featured In Search Results</li>
                                <li>24/7 Support</li>
                            </ul>
                            <a class="button border" href="#">Get Started</a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Row / End -->

    </div>
    <!-- Container / End -->
    <!-- Container / End -->
@stop