@extends('layout.master')
@section('content')
<section class="fullwidth padding-top-75 padding-bottom-90" data-background-color="#f9f9f9">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3 class="headline centered headline-extra-spacing">
                    <strong class="headline-with-separator">What makes us different?</strong>
                    <span class="margin-top-25">There are many groups that connect businesses with experts and investors, but none of them make it easy and with just 1 CL1QUE.</span>
                </h3>
            </div>
        </div>

        <div class="row icons-container">
            <!-- Stage -->
            <div class="col-md-4">
                <div class="icon-box-2 with-line">
                    <img src="images/26.png"/>
                    <p>We provide small businesses with a pipeline of vetted consultants and investors.</p>
                </div>
            </div>

            <!-- Stage -->
            <div class="col-md-4">
                <div class="icon-box-2 with-line">
                    <img src="images/24.png"/>
                    <p>We provide experts with an opportunity to impact a small business with their skills and be compensated for it.</p>
                </div>
            </div>

            <!-- Stage -->
            <div class="col-md-4">
                <div class="icon-box-2">
                    <img src="images/25.png"/>
                    <p>We provide investors with an opportunity to positively impact in the growth of a small business.</p>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-md-offset-2">
            <iframe width="700" height="400" src="https://www.youtube.com/embed/5-mA1onuacQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </div>
</section>
@stop
@section('banner')
<div class="main-search-container full-height alt-search-box centered" >
        <div class="main-search-inner">
            <div class="container">
                <div class="row padding-bottom-90">
                    <div class="col-md-6">
                        <a href="{{route('signup')}}"><img src="images/LOGO-ICONS-3.png"></a>
                    </div>
                    <div class="col-md-6">
                        <img src="images/imageedit_14_3731963023.gif">
                    </div>
                </div>

            </div>

        </div>
    </div>
@stop
@section('flipBanner')
    <a href="listings-half-screen-map-list.html" class="flip-banner parallax" data-background="images/slider-bg-02.jpg" data-color="#f1ae2a" data-color-opacity="0.85" data-img-width="2500" data-img-height="1666">
        <div class="flip-banner-content">
            <h2 class="flip-visible">Connect with Top-Rated Experts</h2>
            <h2 class="flip-hidden">Connect with Investors</h2>
        </div>
    </a>
@stop