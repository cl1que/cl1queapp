@extends('layout.master')
@section('content')
    <div id="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2>Mission</h2>
                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="/">Home</a></li>
                            <li>Mission</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p>Our mission is to match entrepreneurs with the experts and investors they need and make it simple in one click.</p>
            </div>
            <div class="col-md-6">
                <img src="images/IMG_0601.jpg">
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <!-- Container / End -->
@stop