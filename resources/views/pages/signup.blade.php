@extends('layout.master')
@section('content')
        <div id="titlebar">
            <div class="row">
                <div class="col-md-12">
                    <h2>Application</h2>
                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Application</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div id="add-listing">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                        <form action="{{url('signuppost')}}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <!-- Section -->
                    <div class="add-listing-section">

                        <!-- Headline -->
                        <div class="add-listing-headline">
                            <h3><i class="sl sl-icon-doc"></i> Basic Information</h3>
                        </div>

                        <!-- Title -->
                        <div class="row with-forms">
                            <div class="col-md-12">
                                <h5>Name <i class="tip" data-tip-content="Name of your business"></i></h5>
                                <input name="name" class="search-field" type="text" value=""/>
                            </div>
                        </div>

                        <!-- Row -->
                        <div class="row with-forms">

                            <!-- Status -->
                            <div class="col-md-6">
                                <h5>Category</h5>
                                <select class="chosen-select-no-single" >
                                    <option label="blank">Select Category</option>
                                    <option>Business</option>
                                    <option>Expert</option>
                                    <option>Investor</option>
                                </select>
                            </div>

                            <!-- Type -->
                            <div class="col-md-6">
                                <h5>Keywords <i class="tip" data-tip-content="Maximum of 15 keywords related with your business"></i></h5>
                                <input name="keywords" type="text" placeholder="Keywords should be separated by commas">
                            </div>

                        </div>
                        <!-- Row / End -->

                    </div>
                    <!-- Section / End -->

                    <!-- Section -->
                    <div class="add-listing-section margin-top-45">

                        <!-- Headline -->
                        <div class="add-listing-headline">
                            <h3><i class="sl sl-icon-location"></i> Location</h3>
                        </div>

                        <div class="submit-section">

                            <!-- Row -->
                            <div class="row with-forms">

                                <!-- Address -->
                                <div class="col-md-6">
                                    <h5>Address</h5>
                                    <input name="address" type="text" placeholder="e.g. 964 School Street">
                                </div>

                                <!-- City -->
                                <div class="col-md-6">
                                    <h5>City</h5>
                                    <input name="city" type="text" placeholder="City">
                                </div>


                                <!-- City -->
                                <div class="col-md-6">
                                    <h5>State</h5>
                                    <input name="state" type="text">
                                </div>

                                <!-- Zip-Code -->
                                <div class="col-md-6">
                                    <h5>Zip-Code</h5>
                                    <input name="zip" type="text">
                                </div>

                            </div>
                            <!-- Row / End -->

                        </div>
                    </div>
                    <!-- Section / End -->


                    <!-- Section -->
                    <div class="add-listing-section margin-top-45">

                        <!-- Headline -->
                        <div class="add-listing-headline">
                            <h3><i class="sl sl-icon-docs"></i> Details</h3>
                        </div>

                        <!-- Description -->
                        <div class="form">
                            <h5>Description</h5>
                            <textarea class="WYSIWYG" name="summary" cols="40" rows="3" id="summary" spellcheck="true"></textarea>
                        </div>

                        <!-- Row -->
                        <div class="row with-forms">

                            <!-- Phone -->
                            <div class="col-md-4">
                                <h5>Phone <span>(optional)</span></h5>
                                <input name="ziphone" type="text">
                            </div>

                            <!-- Website -->
                            <div class="col-md-4">
                                <h5>Website <span>(optional)</span></h5>
                                <input name="website" type="text">
                            </div>

                            <!-- Email Address -->
                            <div class="col-md-4">
                                <h5>E-mail <span>(optional)</span></h5>
                                <input name="email" type="text">
                            </div>

                        </div>
                        <!-- Row / End -->


                        <!-- Row -->
                        <div class="row with-forms">

                            <!-- Phone -->
                            <div class="col-md-4">
                                <h5 class="fb-input"><i class="fa fa-facebook-square"></i> Facebook <span>(optional)</span></h5>
                                <input name="facebook" type="text" placeholder="https://www.facebook.com/">
                            </div>

                            <!-- Website -->
                            <div class="col-md-4">
                                <h5 class="twitter-input"><i class="fa fa-twitter"></i> Twitter <span>(optional)</span></h5>
                                <input name="twitter" type="text" placeholder="https://www.twitter.com/">
                            </div>

                            <!-- Email Address -->
                            <div class="col-md-4">
                                <h5 class="gplus-input"><i class="fa fa-google-plus"></i> Google Plus <span>(optional)</span></h5>
                                <input name="google" type="text" placeholder="https://plus.google.com">
                            </div>

                        </div>
                        <!-- Row / End -->
                    </div>
                    <!-- Section / End -->
                        <button class="button preview">
                            Submit <i class="fa fa-arrow-circle-right"></i>
                        </button>
                    </form>
                </div>
            </div>

            <!-- Copyrights -->
            <div class="col-md-12">
                <div class="copyrights">© 2019 Listeo. All Rights Reserved.</div>
            </div>

        </div>
@stop