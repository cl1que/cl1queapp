@extends('layout.master')
@section('content')
    <div id="titlebar">
        <div class="container">
            <div class="row">
                <div class="col-md-12">

                    <h2>Matches</h2>

                    <!-- Breadcrumbs -->
                    <nav id="breadcrumbs">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li>Matches</li>
                        </ul>
                    </nav>

                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                <!-- Filters -->
                <div id="filters">
                    <ul class="option-set margin-bottom-30">
                        <li><a href="#filter" class="selected" data-filter="*">All</a></li>
                        <li><a href="#filter" data-filter=".first-filter">Good Matches</a></li>
                        <li><a href="#filter" data-filter=".second-filter">Favorites</a></li>
                        <li><a href="#filter" data-filter=".third-filter">Top Rated</a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

            </div>
        </div>

        <div class="row">

            <!-- Projects -->
            <div class="projects isotope-wrapper">
                @foreach($matches as $match)
                    <div class="col-lg-4 col-md-6 isotope-item">
                        <a href="/detail" class="listing-item-container compact">
                            <div class="listing-item">
                                <img src="images/listing-item-01.jpg" alt="">
                                @if($match->badge === "good")
                                    <div class="listing-badge now-open">Great Match</div>
                                @endif
                                @if($match->badge === "bad")
                                    <div class="listing-badge now-closed">Bad Match</div>
                                @endif
                                <div class="listing-item-content">
                                    <div class="numerical-rating" data-rating="{{ $match->rating }}"></div>
                                    <h3>{{ $match->title }}</h3>
                                    <span>{{ $match->subtitle }}</span>
                                </div>
                                <span class="like-icon"></span>
                            </div>
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@stop