<?php

namespace App\Http\Controllers;

use App\Matches;
use View;
use Illuminate\Http\Request;
use App\Application;

class PagesController extends Controller
{
    public function homepage()
    {
        return View::make('pages.home');
    }

    public function contact()
    {
        return View::make('pages.contact');
    }

    public function match()
    {
        $matches = Matches::all();
        return View::make('pages.match',compact('matches'));
    }

    public function mission()
    {
        return View::make('pages.mission');
    }

    public function pricing()
    {
        return View::make('pages.pricing');
    }

    public function profile()
    {
        return View::make('pages.pricing');
    }

    public function signup()
    {
        return View::make('pages.signup');
    }

    public function signuppost(Request $request)
    {
        Application::create($request->all());
        return redirect('match');
    }

    public function solution()
    {
        return View::make('pages.solution');
    }

    public function detail()
    {
        return View::make('pages.detail');
    }
}
